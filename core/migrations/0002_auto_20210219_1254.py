# Generated by Django 3.1.6 on 2021-02-19 12:54

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
            ],
            options={
                'verbose_name': 'Таск для смс',
                'verbose_name_plural': 'Таски для смс',
            },
        ),
        migrations.RemoveField(
            model_name='order',
            name='sms_status',
        ),
    ]
