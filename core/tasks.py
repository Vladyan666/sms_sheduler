# from datetime import timedelta
import requests
from django.db.models import Q
import datetime
from celery.decorators import periodic_task
from celery.schedules import crontab
from django.conf import settings
from requests.exceptions import ConnectionError
from django.template.loader import render_to_string
from .views import *
from _project_.celery import celery_app


@celery_app.task(bind=True, queue='sms_q')
def checkOrders(self):
    data = CheckNewOrders()
    return {'orders': data}

@celery_app.task(bind=True, autoretry_for=(Exception, ConnectionError, ), retry_kwargs={'max_retries': 3, 'countdown': 1}, queue='sms_q')
def createSmsTask(self):
    data = CreateSmsTask()
    
    return {'sms_create': data}

@celery_app.task(bind=True, autoretry_for=(Exception, ConnectionError, ), retry_kwargs={'max_retries': 3, 'countdown': 1}, queue='sms_q')
def getSmsTask(self):
    data = checkSimFromTasks()
    
    return {'sms_create': data}

@celery_app.task(bind=True, autoretry_for=(Exception, ConnectionError, ), retry_kwargs={'max_retries': 3, 'countdown': 1}, queue='sms_q')
def extractSmsTask(self):
    data = GetSMS()
    
    return {'sms_create': data}

@celery_app.task(bind=True, autoretry_for=(Exception, ConnectionError, ), retry_kwargs={'max_retries': 3, 'countdown': 1}, queue='sms_q')
def checkSmsTask(self):
    data = CheckSMS()
    
    return {'sms_create': data}