from django.contrib import admin

from .models import *

class OrderAdmin(admin.ModelAdmin):
    list_display = ['fiat_amount', 'requisities', 'fromreq', 'order_id', 'status', 'token']

admin.site.register(Order, OrderAdmin)

class TaskAdmin(admin.ModelAdmin):
    list_display = ['sms_hash', 'order', 'status', 'attempt']
    search_fields = ['sms_hash', 'order__order_id']

admin.site.register(Task, TaskAdmin)

class SmsAdmin(admin.ModelAdmin):
    list_display = ['phone', 'sender']

admin.site.register(Sms, SmsAdmin)
