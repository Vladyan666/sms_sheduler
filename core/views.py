import requests
import json
import datetime
import re
import pytz
from dateutil.parser import parse as date_parser
from django.conf import settings as django_settings
from .models import Task, Order, Sms
from django.db.models import Q

SMB_HEDAERS = django_settings.SMS_HOST_TOKEN #{'Authorization': 'Token b194457177bafefa4ec5ac697f0d0c3c826089a8'}
#DAAZ_HEADERS = {'Authorization': 'Bearer c59ea59a-aecb-42e0-aff0-408d549185f9'}
DAAZ_HEADERS = django_settings.DAAZ_TOKEN #{'Authorization': 'Bearer 4a872cef-a16e-4b57-9b80-a12a4469fcc3'}

def _json_request(url, params=None, headers=None, json=None, method='GET', request=None, **kwargs):
    r = requests.request(method, url, params=params, json=json, headers=headers, **kwargs, timeout=3)
    return r.json()

def _status_request(url, params=None, headers=None, json=None, method='GET', request=None, **kwargs):
    r = requests.request(method, url, params=params, json=json, headers=headers, **kwargs, timeout=3)
    return r.status_code


def closeTask(t):
    headers = SMB_HEDAERS
    # но если есть еще таски с этим хешом, то таск не закрываем
    if not Task.objects.filter(sms_hash=t.sms_hash, status__in=[1,2]).exists():
        _json_request(django_settings.SMS_HOST + '/api/sim/remove_sim_on_goip/', method='POST', headers=headers, data={'sim': t.phone_slot_id})

def CheckNewOrders():
    """
        Создание новых ордеров
    """

    url = 'https://daazweb.space/api/getOrders'
    headers = DAAZ_HEADERS
    new_orders = []
    #data = {'orders': [{'id': 123111, 'status': 'Finished'}]}
    obtained_orders = []
    for header in headers:
        data = _json_request(url, headers={'Authorization': f'Bearer {header}'})
        for item in data['orders']:
            # проверяем существование заказа
            if Order.objects.filter(order_id=item['id']).exists() and item['status'] != 'On checking':
                # если статус изменился - меняем
                if Order.objects.get(order_id=item['id']).status != item['status']:
                    Order.objects.filter(order_id=item['id']).update(status=item['status'])

                # пропускаем если так
                if item['status'] in ['Seller requisite', 'Paid']:
                    continue

                # если таск не завершен успешно
                if not Task.objects.filter(order__order_id=item['id'], status=3).exists():
                    if Task.objects.filter(order__order_id=item['id']).exists():
                        t = Task.objects.get(order__order_id=item['id'])
                        t.status = 4
                        t.save()
                        try:
                            closeTask(t)
                        except:
                            pass
            elif item['status'] != 'On checking':
                if item['status'] in ['Seller requisite', 'Paid']:
                    order = {
                        'fiat_amount': item['fiat_amount'],
                        'requisities': item['requisities'],
                        'fromreq': item['fromreq'],
                        'order_id': item['id'],
                        'status': item['status'],
                        'datetime': item['created_at'],
                        'token': header
                    }

                    Order.objects.create(**order)
                    new_orders.append(order)

            obtained_orders.append(item['id'])
    
    for header in headers:
        for item in Order.objects.filter(~Q(order_id__in=obtained_orders), status__in=['Paid', 'Seller requisite']):
            url = f'https://daazweb.space/api/getOrderById/?order_id={item.order_id}'
            data = _json_request(url, headers=header)
            if data['data']['status'] != item.status:
                item.status = data['data']['status']
                item.save()

    return new_orders


def CreateSmsTask():
    # Берем телефоны заказов со статусом in ['Seller requisite', 'Paid']
    url = django_settings.SMS_HOST + '/api/sim/get_sims/'
    headers = SMB_HEDAERS

    phones = []
    orders_dict = []
    for i in Order.objects.filter(status__in=['Seller requisite', 'Paid']):
        phones.append(re.sub('\D', '', i.requisities))
        orders_dict.append({
            'phone': re.sub('\D', '', i.requisities),
            'order': i.order_id
        })

    # Если есть активные ордера запускаем таски

    created_tasks = []  
    if phones:
        ids = []
        data = _json_request(url, headers=headers)

        url = django_settings.SMS_HOST + '/api/task/'

        # Создание тасков
        for item in data:
            if item['name'] in phones:
                data = {'phone': item['name'], 'lifetime': 180}
                for ordr in orders_dict:
                    if ordr['phone'] == item['name']:
                        t, cr = Task.objects.get_or_create(order=Order.objects.get(order_id=ordr['order']), phone_id_smb=item['pk'], defaults={'phone_slot_id': item['slot']})
                        if not t.status in [3, 4]:
                            req = _json_request(url, headers=headers, method='POST', data=data)
                            if req.get('error') == "Sim already used":
                                # если симка уже используется пробуем взять хэш у соседней сессии, если их нет - плохо!
                                if Task.objects.filter(phone_id_smb=item['pk'], order__status__in=['Seller requisite', 'Paid'], sms_hash__isnull=False).exists():
                                    t.sms_hash = Task.objects.filter(phone_id_smb=item['pk'],
                                        order__status__in=['Seller requisite', 'Paid'],
                                        sms_hash__isnull=False).values_list('sms_hash', flat=True)[0]
                                    t.save()
                                    created_tasks.append(t.id)
                                else:
                                    continue
                            
                            if req.get('message'):
                                t.sms_hash = req['message']['hash']
                                t.save()
                                created_tasks.append(t.id)

                                url = django_settings.SMS_HOST + '/api/sim/check_goip_slot_sim/'
                                req2 = _status_request(url, method='POST', headers=headers, data={'task': req['message']['hash']})
                                if not req2 == 200:
                                    continue

    return created_tasks


def checkSimFromTasks(hsh=None):
    """
        Доводим таски до получения СМС
    """

    headers = SMB_HEDAERS

    task_launch = []
    task_pre_launch = []
    queryset = Task.objects.filter(sms_hash=hsh) if hsh else Task.objects.filter(status=1, order__status__in=['Seller requisite', 'Paid'])
    for t in queryset:

        if (datetime.datetime.strptime(t.order.datetime, '%d.%m.%y, %H:%M') + datetime.timedelta(minutes=40)) < (datetime.datetime.now() + datetime.timedelta(hours=2, minutes=40)):
            t.status = 4
            t.order.status = 'Error'
            t.order.save()
            t.save()
            closeTask(t)
            continue
        
        if t.sms_hash:
            if t.attempt > 9:
                t.status = 4
                t.attempt = 0
                t.save()
                closeTask(t)
                t.status = 1
                t.sms_hash = None
                t.save()
                continue


            url = django_settings.SMS_HOST + '/api/task/current_status/'
            req2 = _json_request(url, method='POST', headers=headers, data={'task': t.sms_hash})

            if req2.get('message') in [3]:
                t.status = 2
                t.save()
                task_launch.append(t.sms_hash)

            t.attempt = t.attempt + 1
            t.save()

        else:
            url = django_settings.SMS_HOST + '/api/task/'
            data = {'phone': re.sub('\D', '', t.order.requisities), 'lifetime': 180}
            req = _json_request(url, headers=headers, method='POST', data=data)

            if req.get('error') == "Sim already used":
                # если симка уже используется пробуем взять хэш у соседней сессии, если их нет - плохо!
                if Task.objects.filter(phone_id_smb=t.phone_id_smb, order__status__in=['Seller requisite', 'Paid'], sms_hash__isnull=False).exists():
                    t.sms_hash = Task.objects.filter(phone_id_smb=t.phone_id_smb,
                        order__status__in=['Seller requisite', 'Paid'],
                        sms_hash__isnull=False).values_list('sms_hash', flat=True)[0]
                    t.save()
                    
                else:
                    continue
            
            if req.get('message'):
                t.sms_hash = req['message']['hash']
                t.save()
                
                url = django_settings.SMS_HOST + '/api/sim/check_goip_slot_sim/'
                req2 = _status_request(url, method='POST', headers=headers, data={'task': req['message']['hash']})
                if not req2 == 200:
                    continue

    return {'pre': task_pre_launch, 'laun': task_launch}


def GetSMS():

    """
        Получаем СМС
    """
    recieve_type = 'goip'
    errors = []
    success = []
    headers = SMB_HEDAERS
    for t in Task.objects.filter(status=2):

        if (datetime.datetime.strptime(t.order.datetime, '%d.%m.%y, %H:%M') + datetime.timedelta(minutes=40)) < (datetime.datetime.now() + datetime.timedelta(hours=2, minutes=40)):
            t.status = 4
            t.order.status = 'Error'
            t.order.save()
            t.save()
            closeTask(t)
            continue

        # если заказ в других статусах - закрываем таск
        if not t.order.status in ['Seller requisite', 'Paid']:
            t.status = 4
            t.save()
            closeTask(t)
            continue

        # если заказ оплачен проверяем его
        if t.sms_hash and t.order.status in ['Paid', 'Seller requisite']:
            url = django_settings.SMS_HOST + '/api/goip/get_sms/?hash=' + t.sms_hash
            data = _json_request(url, headers=headers)
            #data = {'message': {'sms': [{"content":"79276851162 vam 1111.00 rub","sms_sender":"2021-03-10 05:44:24","datetime":"QIWIWallet"}]}}
            if len(data.get('message')) > 20:
                if data.get('message', {}):
                    for i in json.loads(data.get('message')):
                        if t.order.fromreq in i['content'] and i['datetime'] == 'QIWIWallet':
                            if datetime.datetime.strptime(t.order.datetime, '%d.%m.%y, %H:%M') < datetime.datetime.strptime(i['sms_sender'], '%Y-%m-%d %H:%M:%S') + datetime.timedelta(hours=3):
                                q = re.search('vam (.*?) rub', i['content'])
                                if q:
                                    if int(float(q.group(1))) >= int(float(t.order.fiat_amount)):
                                        if t.order.status == 'Paid':
                                            t.status = 3
                                            t.success_mesage = i
                                            t.save()
                                            closeTask(t)
                                        # отправляем в сервис пометку
                                        requests.get('http://daazweb.com/notification_bot?token={}&number={}&text={}&call_type=qiwisms&bank=qiwi&sender={}'.format(t.order.token, re.sub('\D', '', t.order.requisities), int(float(q.group(1))), re.sub('\D', '', t.order.fromreq)))
                                        # но если есть еще таски с этим хешом, то таск не закрываем
                                        success.append(i)

                                q = re.search('отправил вам (.*?) руб.', i['content'])
                                if q:
                                    if int(float(q.group(1))) >= int(float(t.order.fiat_amount)):
                                        if t.order.status == 'Paid':
                                            t.status = 3
                                            t.success_mesage = i
                                            t.save()
                                            closeTask(t)
                                        # отправляем в сервис пометку
                                        requests.get('http://daazweb.com/notification_bot?token={}&number={}&text={}&call_type=qiwisms&bank=qiwi&sender={}'.format(t.order.token, re.sub('\D', '', t.order.requisities), int(float(q.group(1))), re.sub('\D', '', t.order.fromreq)))
                                        # но если есть еще таски с этим хешом, то таск не закрываем
                                        success.append(i)

                        # elif i['datetime'] == 'QIWIWallet' and Task.objects.filter(sms_hash=t.sms_hash, status__in=[1,2]).count() == 1:
                        #     if Sms.objects.filter(datetime=datetime.datetime.strptime(i['sms_sender'], '%Y-%m-%d %H:%M:%S'), text=i['content']):
                        #     if datetime.datetime.strptime(t.order.datetime, '%d.%m.%y, %H:%M') < datetime.datetime.strptime(i['sms_sender'], '%Y-%m-%d %H:%M:%S'):
                        #         q = re.search('vam (.*?) rub', i['content'])
                        #         if q:
                        #             if int(float(q.group(1))) == int(float(t.order.fiat_amount)):
                        #                 if t.order.status == 'Paid':
                        #                     t.status = 3
                        #                     t.success_mesage = i
                        #                     t.save()
                        #                     closeTask(t)
                        #                 # отправляем в сервис пометку
                        #                 requests.get('http://daazweb.com/notification_bot?token=4a872cef-a16e-4b57-9b80-a12a4469fcc3&number={}&text={}&call_type=qiwisms&bank=qiwi&sender={}'.format(re.sub('\D', '', t.order.requisities), int(float(q.group(1))), re.sub('\D', '', t.order.fromreq)))
                        #                 # но если есть еще таски с этим хешом, то таск не закрываем
                        #                 success.append(i)


    return {'success': success, 'errors': errors}




# перебор и создание смс
def _cycle_items(item, phone):
    to_create = []
    for it in item:
        if it:
            text = ''
            for i in range(2, len(it.split(','))):
                text += it.split(',')[i]
            params = dict(phone=phone.id, sender=it.split(',')[1], text=text, datetime=date_parser(it.split(',')[0]))
            if not Sms.objects.filter(**params).exists():
                to_create.append(Sms(**params))

    Sms.objects.bulk_create(to_create)


def task(request):

    # запрос
    def main_req():
        url = django_settings.GOIP_HOST + '/default/en_US/tools.html?type=sms_inbox'
        r = requests.get(url, auth=('admin', django_settings.GOIP_PASSWORD))
        return r


    try:
        r = main_req()
    except:
        pass
        # auth_req()
        # r = main_req()

    r.encoding = 'utf-8'

    # содержимое страницы
    text = re.sub('[\n|\r|\t]', '', r.text)
    text = text.replace('\r\n', '')
    new_t = re.search('pos=0\;sms=(.*?);var line_obj', text).group(1)
    phones = re.split(';sms_row_insert\(.*?\);sms= ', new_t)

    return phones
    count = 1
    # перебор телефонов
#     for i in phones:
#         if count != 8:
# #                 try:
#             temp = json.loads(i.split(';sms_row_insert')[0], strict=False)
#             _cycle_items(temp, phone)
# #                 except Exception as e:
# #                     pass
#         else:
# #                 try:
#             temp = json.loads(i.split(';sms_row_insert')[0], strict=False)
#             _cycle_items(temp, phone)
# #                 except Exception as e:
# #                     pass

#         count += 1

#     return HttpResponse('done')

def build_date(dt):
    return datetime.datetime(hour=dt.hour, year=dt.year, month=dt.month, minute=dt.minute, second=dt.second, day=dt.day)

def CheckSMS():

    """
        Получаем СМС
    """

    type_receive = django_settings.TYPE_RECEIVE

    r1 = requests.get(django_settings.SMS_HOST + '/api/goip/', headers=django_settings.SMS_HOST_TOKEN, timeout=2).json()

    sms_data = []

    to_create = []
    if type_receive == 'goip':
        tmp_data = task(1)
        for i in r1['message']:
            if i['line_id'] and i['phone_number']:
                for sms in json.loads(tmp_data[int(i['line_id']) - (101 % 100)].split(';sms_row_insert')[0], strict=False):
                    if sms:
                        text = ''
                        for spl in range(2, len(sms.split(','))):
                            text += sms.split(',')[spl]
                        params = dict(phone=i['phone_number'], sender=sms.split(',')[1], text=text, datetime=date_parser(sms.split(',')[0]))
                        tzinfo = datetime.timezone(datetime.timedelta(hours=3))
                        if build_date((datetime.datetime.now(tzinfo) - datetime.timedelta(seconds=300))) < build_date(pytz.utc.localize(date_parser(sms.split(',')[0]))):
                            if not Sms.objects.filter(**params).exists():
                                to_create.append(Sms(**params))

        Sms.objects.bulk_create(to_create)
    else:
        for i in r1['message']:
            if i['line_id'] and i['phone_number']:
                url = django_settings.SMS_SERVER
                sms_date = datetime.datetime.now(datetime.timezone(datetime.timedelta(hours=django_settings.TZ_DELTA, minutes=-5))).strftime('%Y-%m-%d %H:%M:%S')
                data = {'action': 'get_sms_new', 'lefttime': 5, "goipid": i['line_id'], "date": sms_date}

                r_sms = requests.post(url, data=data).text

                smses = r_sms.split("/;/")

                if smses[0] != '':
                    for s in smses:
                        temp = s.split("/,/")
                        dt = datetime.datetime.fromisoformat(temp[1])
                        if not Sms.objects.filter(datetime=dt, sender=temp[0], text=temp[2]).exists():
                            to_create.append(Sms(datetime=dt, phone=i['phone_number'], sender=temp[0], text=temp[2]))

        Sms.objects.bulk_create(to_create)
    for t in to_create:
        if re.findall("otpravil vam .*? rub|Spisanie c .*? na summu|Oplata\. Kod\: .*?\. Summa|отправил вам .*? руб\.|Списание с .*? Получатель", t.text):
            requests.get(f"https://api.telegram.org/bot{django_settings.TELEGRAM_BOT}/sendMessage?chat_id={django_settings.TELEGRAM_PAY}&text=⏰{t.datetime.strftime('%d-%m %H:%M:%S')}\n☎️{t.phone}\n📻{t.sender}\n📩{t.text}")
        else:
            requests.get(f"https://api.telegram.org/bot{django_settings.TELEGRAM_BOT}/sendMessage?chat_id={django_settings.TELEGRAM_OTHER}&text=⏰{t.datetime.strftime('%d-%m %H:%M:%S')}\n☎️{t.phone}\n📩{t.text}")

    return len(to_create)
