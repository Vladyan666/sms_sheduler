from django.db import models

# Create your models here.

class Order(models.Model):

    class Meta:
        verbose_name = u"Заказ"
        verbose_name_plural = u"Заказы"

    fiat_amount = models.FloatField("Сумма")
    requisities = models.CharField("Реквизиты платежа", max_length=80)
    fromreq = models.CharField("Реквизиты покупателя", max_length=80)
    order_id = models.CharField("Номер заказа", max_length=80)
    status = models.CharField("Статус заказа", max_length=80)
    datetime = models.CharField("Время заказа", max_length=80, null=True, blank=True)
    finished = models.BooleanField("Обработан", default=False)
    token = models.CharField("Токен", max_length=100, null=True, blank=True)

    def __str__(self):
        return str(self.pk)


TASK_STATUS = (
    (1, 'Создано'),
    (2, 'Ожидаение СМС'),
    (3, 'Выполненно'),
    (4, 'Ошибка')
)

class Task(models.Model):

    class Meta:
        verbose_name = "Таск для смс"
        verbose_name_plural = "Таски для смс"

    order = models.ForeignKey(Order, verbose_name="Заказ", on_delete=models.CASCADE, null=True, blank=True)
    phone_id_smb = models.IntegerField('ID телефона в симбанке', default=-1)
    phone_slot_id = models.IntegerField('ID слота телефона в симбанке', default=-1)
    sms_hash = models.CharField('Хэш таска', max_length=200, null=True, blank=True)
    status = models.PositiveSmallIntegerField('Статус', choices=TASK_STATUS, default=1)
    attempt = models.IntegerField("Количество попыток вставить сим карту", default=0)
    success_mesage = models.TextField("Текст сообщения", null=True, blank=True)

    def __str__(self):
        return str(self.pk)


class Sms(models.Model):

    class Meta:
        verbose_name = u"Смс"
        verbose_name_plural = u"Смс"

    text = models.TextField('Текст')
    sender = models.CharField('Отправитель', max_length=100)
    datetime = models.DateTimeField('Дата')
    phone = models.CharField('Телефон', max_length=100)

    def __str__(self):
        return str(self.id)