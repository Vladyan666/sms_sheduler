import os  
from celery import Celery
os.environ.setdefault('DJANGO_SETTINGS_MODULE', '_project_.settings')
celery_app = Celery('core')  
celery_app.config_from_object('django.conf:settings', namespace='CELERY')  
celery_app.autodiscover_tasks()

celery_app.conf.beat_schedule = {
    "check-orders": {
        "task": "core.tasks.checkOrders",
        "schedule": 30.0
    },
    "check-smses": {
        "task": "core.tasks.createSmsTask",
        "schedule": 30.0
    },
    "get-smses": {
        "task": "core.tasks.getSmsTask",
        "schedule": 30.0
    },
    "extract-sms": {
        "task": "core.tasks.extractSmsTask",
        "schedule": 30.0
    },
#    "check-sms": {
#        "task": "core.tasks.checkSmsTask",
#        "schedule": 30.0
#    }
}